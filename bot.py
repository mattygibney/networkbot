import telegram
from telegram.ext import CommandHandler, Updater, MessageHandler, Filters
import speedtest
import time
from graph import read_graph
#Read telegram api token
token_file = open("/home/pi/networkbot/TOKEN.txt")
token_value = token_file.read().replace("\n", "")
token_file.close()

updater = Updater(token=token_value, use_context=True)

def start(update, context):
        text_to_send = """Welcome to the info bot
                          /run to run speedtest
                          /graph for average speed graph"""

        context.bot.send_message(chat_id=update.effective_chat.id,text=text_to_send)

def unknown(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id,text="Sorry that was and incorrect command")

def log(down, upload):
    epoch = int(time.time())
    log_file = open("/home/pi/networkbot/log.txt", "a")
    log_file.write(str(epoch) + "," + str(down) + "," + str(upload) + "\n")
    log_file.close()

def run(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Running speed test now, may take 30s\n")
    s = speedtest.Speedtest()
    s.get_servers()
    s.get_best_server()
    s_down = s.download()
    s_up = s.upload()

    log(s_down,s_up)
    text_to_send = "Download: " + str( s_down / 1000000 ) + "\nUpload: " + str(s_up / 1000000) + "\n"
    context.bot.send_message(chat_id=update.effective_chat.id, text=text_to_send)

def graph(update, context):
    #context.bot.send_photo(chat_id=update.effective_chat.id, photo='https://telegram.org/img/t_logo.png')
    imagename = read_graph("/home/pi/networkbot/log.txt")
    image = open(imagename,'rb')
    context.bot.send_photo(chat_id=update.effective_chat.id, photo=image)
    image.close()


graph_handler = CommandHandler('graph',graph)
run_handler = CommandHandler('run', run)        
start_handler = CommandHandler('start',start)

updater.dispatcher.add_handler(run_handler)
updater.dispatcher.add_handler(start_handler)
updater.dispatcher.add_handler(graph_handler)
updater.start_polling()
