import matplotlib.pyplot as plt
import csv
import datetime

def draw_graph(x,y, filename):
    for i in range(0,len(x)):
        x[i] = datetime.datetime.fromtimestamp(int(x[i]))
    for i in range(0, len(y)):
        y[i] = float(y[i]) / 1000000

    plt.plot(x,y)
    plt.savefig("/home/pi/networkbot/test.png")
    return "test.png"

def read_graph(filename):
    times = []
    up = []
    down = []
    data_file=open(filename, "r")
    reader = csv.reader(data_file, delimiter=',')
    for row in reader:
        times.append(row[0])
        up.append(row[1])
        down.append(row[2])

    imagename = draw_graph(times,up,filename)
    return imagename



def main():
    print(read_graph("/home/pi/networkbot/log.txt"))

if __name__ == '__main__':
    main()
